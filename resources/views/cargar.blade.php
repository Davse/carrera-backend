@extends('menubackend')

@section('card-header')
Cargar pregunta
@endsection

@section('card-body')

    <div>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    
    <form action="/preguntas/guardar" method="POST">
    {{ csrf_field() }}
        <fieldset>
            <label for="dificultad" id="dificultad" required>Dificultad</label>
            <select name="dificultad" id="dificultad">
                <option value="baja">Baja</option>
                <option value="media">Media</option>
                <option value="dificil">Difícil</option>
            </select>
            <br>
            <label for="pregunta">Pregunta</label>
            <textarea id="pregunta" name="pregunta" required></textarea>
            <br>
            <label for="respuesta1" id="respuesta1">Respuesta</label>
            <textarea id="respuesta1" name="respuesta1" required></textarea>
            <input type="radio" name="respuesta" id="correcta1" value="1" checked>
            <label for="correcta1">Correcta</label>
            <br>
            <label for="respuesta2" id="respuesta2">Respuesta</label>
            <textarea id="respuesta2" name="respuesta2" required></textarea>
            <input type="radio" name="respuesta" id="correcta2" value="2">
            <label for="correcta2">Correcta</label>
            <br>
            <button>Enviar</button>
    </fieldset>   
    </form>
@endsection