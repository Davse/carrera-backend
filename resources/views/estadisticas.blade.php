@extends('menubackend2')

@section('card-header')
Estadísticas
@endsection

@section('card-body')
    <p><a class="boton derecha" href="/estadisticas/csv">Descargar csv</a></p>
    <br><br>
    <table class="tablapreg">
        <thead>
            <th>Pregunta</th>
            <th>Autor</th>
            <th>Último Modificador</th>
            <th>Dificultad</th>
            <th>Apariciones</th>
            <th>Bien Contestada</th>
        </thead>
        <tbody>
        @foreach ($preguntas as $pregunta)
            <tr>
                <td><a href= "/estadisticas/pregunta/{{ $pregunta->id }}">{{ $pregunta->pregunta }}</a></td>
                <td>{{ $pregunta->creador->name }}</td>
                <td>{{ $pregunta->modificador->name }}</td>
                <td>{{ $pregunta->dificultad }}</td>
                <td>{{ $pregunta->partidas->count() }}</td>
                <td>{{ $pregunta->veces_bien_contestada }}</td>
                
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection


@section('card2-header')
Partidas
@endsection
@section('card2-body')
<p>Jugadas en fácil: {{  DB::table('partidas')
                                        ->where('dificultad', '=', 'baja')
                                        ->distinct('id')
                                        ->count()   }}</p>
<p>Jugadas en medio: {{  DB::table('partidas')
                                        ->where('dificultad', '=', 'media')
                                        ->distinct('id')
                                        ->count()   }}</p>
<p>Jugadas en difícil: {{  DB::table('partidas')
                                        ->where('dificultad', '=', 'dificil')
                                        ->distinct('id')
                                        ->count()   }}</p>
<p>Total jugadas: {{  DB::table('partidas')
                                        ->distinct('id')
                                        ->count()   }}</p>
<p>Finalizadas: {{  DB::table('partidas')
                                        ->where('duracion', '<>', 0)
                                        ->distinct('id')
                                        ->count()   }} </p>
<p>Sin terminar: {{  DB::table('partidas')
                                        ->where('duracion', '=', 0)
                                        ->distinct('id')
                                        ->count()   }}</p>
<p>Duración promedio:  <?php $avg = DB::table('partidas')
                                        ->where('duracion', '<>', 0)
                                        ->distinct('id')
                                        ->avg('duracion')   ?> {{number_format($avg/60,0)}} mins.</p>
@endsection