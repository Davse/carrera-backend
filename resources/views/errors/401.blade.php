@extends('menubackend')

@section('card-header')
Acción no permitida
@endsection

@section('card-body')
    <p>{{ $exception->getMessage()}}</p>
    <a href="/home">Volver</a>
@endsection