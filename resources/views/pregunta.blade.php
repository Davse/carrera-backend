@extends('menubackend')

@section('card-header')
Ver pregunta
@endsection

@section('card-body')
    <form action="/estadisticas" method="GET">
    {{ csrf_field() }}
        <ul>
        <li>Pregunta: {{ $pregunta->pregunta }}</li>
        @foreach ($respuestas as $respuesta)
            <li>Respuesta {{ $loop->iteration }}: 
            @if ($respuesta->es_correcta) 
                <span class="correcta">{{ $respuesta->respuesta }}</span> (correcta)
            @else
                <span class="incorrecta">{{ $respuesta->respuesta }}</span>
            @endif
            </li>
        @endforeach
        <li>Dificultad: {{ $pregunta->dificultad }}</li>
        <li>Apariciones: {{ $pregunta->partidas->count() }}</li>
        <li>Veces Bien Contestada: {{ $pregunta->veces_bien_contestada }}</li>
        </ul>
        <button type="submit" name="volver" id="volver" value="volver">Volver</button>
    </form>
@endsection