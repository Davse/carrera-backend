@extends('menubackend')

@section('card-header')
Cargar pregunta
@endsection

@section('card-body')


    <div>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    
    <form action="/preguntas/guardar/{{ $pregunta->id }}" method="POST">
    {{ csrf_field() }}
        <fieldset>
            <label for="dificultad" id="dificultad" required>Dificultad</label>
            <select name="dificultad" id="dificultad">
               @if ( $pregunta->dificultad == 'baja')
                <option value="baja" selected>Baja</option>
               @else
                <option value="baja">Baja</option>
               @endif
               @if ($pregunta->dificultad == 'media')
                <option value="media" selected>Media</option>
               @else
                <option value="media">Media</option>
               @endif
               @if ($pregunta->dificultad == 'dificil')
                <option value="dificil" selected>Difícil</option>
               @else  
                <option value="dificil">Difícil</option>
               @endif
            </select>
            <br>
            <label for="pregunta">Pregunta</label>
            <textarea id="pregunta" name="pregunta" required>{{ $pregunta->pregunta }}</textarea>
            <br>
            <label for="respuesta1" id="respuesta1">Respuesta</label>
            <textarea id="respuesta1" name="respuesta1" required>{{ $respuestas[0]->respuesta }}</textarea>
            @if ($respuestas[0]->es_correcta == 1)
                <input type="radio" name="respuesta" id="correcta1" value="1" checked>
            @else 
                <input type="radio" name="respuesta" id="correcta1" value="1">
            @endif    
            <label for="correcta1">Correcta</label>
            <br>

            
            <label for="respuesta2" id="respuesta2">Respuesta</label>
            <textarea id="respuesta2" name="respuesta2" required>{{ $respuestas[1]->respuesta }}</textarea>
            @if ($respuestas[1]->es_correcta == 1)
                <input type="radio" name="respuesta" id="correcta2" value="2" checked>
            @else 
                <input type="radio" name="respuesta" id="correcta2" value="2">
            @endif
            <label for="correcta2">Correcta</label>
            <br><br>
            <p>Pregunta cargada por: {{ $pregunta->creador->name }}</p>
            <p>Pregunta editada por última vez por: {{ $pregunta->modificador->name }}</p>
            <button name="guardar" id="guardar" value="guardar">Guardar</button>
            <button name="cancelar" id="cancelar" value="Cancelar">Cancelar</button>
    </fieldset>   
    </form>
@endsection