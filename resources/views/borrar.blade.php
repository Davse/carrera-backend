@extends('menubackend')

@section('card-header')
Eliminación de pregunta
@endsection

@section('card-body')
    <form action="/preguntas" method="GET">
        <p>Eliminación exitosa</p>
        <button type="submit" name="volver" id="volver" value="volver">Volver</button>
    </form>
@endsection