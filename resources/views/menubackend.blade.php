@extends('layouts.app')

@section('menu-backend')
<li class="nav-item"><a class="nav-link" href="/preguntas">Preguntas</a></li>
<li class="nav-item"><a class="nav-link" href="/estadisticas">Estadisticas</a></li>
<li class="nav-item"><a class="nav-link" href="/usuarios">Usuarios</a></li>
<li class="nav-item"><a class="nav-link" href="/casilleros">Casilleros</a></li>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@yield('card-header')</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @yield('card-body')
                </div>
            </div>
        </div>        
    </div>
</div>
@endsection
