@extends('menubackend')

@section('card-header')
Administración de Usuarios
@endsection

@section('card-body')
    <a class="boton derecha" href= "/usuarios/cargar">Cargar nuevo Usuario</a>
    <br>
    <br>
    <table class="tablapreg">
        <thead>
            <th>Usuario</th>
            <th>Roles</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </thead>
        <tbody>
        @foreach ($usuarios as $usuario)
            <tr>

            @if ($usuario->activo)
                    <td>{{ $usuario->name }}</td>
                    <td>
                    @foreach ($usuario->roles as $rol)
                        {{ $rol->nombre }} 
                    @endforeach
                    </td>
                    <td><a href= "/usuarios/editar/{{ $usuario->id }}">Editar</a></td>
                    <td>
                    @if (!$usuario->roles->contains($rol_admin))
                        <a href= "/usuarios/desactivar/{{ $usuario->id }}">Desactivar</a>
                    @endif
                    </td>        
            @else  
                    <td class="inactiva">{{ $usuario->name }}</td>
                    <td>
                    @foreach ($usuario->roles as $rol)
                        {{ $rol->nombre }} 
                    @endforeach
                    </td>
                    <td class="inactiva">Editar</td>
                    <td><a href= "/usuarios/activar/{{ $usuario->id }}">Activar</a></td>
            @endif
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection