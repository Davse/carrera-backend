@extends('menubackend')

@section('card-header')
Editar usuario
@endsection

@section('card-body')

    <div>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    
    <form action="/usuarios/guardar/{{ $usuario->id }}" method="POST">
    {{ csrf_field() }}
    <fieldset>
        <div>
            <label for="name">Nombre</label>
            <input type="text" name="name" id="name" value="{{ $usuario->name }}" required>
        </div>
        <div>
            <label for="email">E-Mail</label>
            <input type="email" name="email" id="email" value="{{ $usuario->email }}" required>
        </div>
        <div>
            @foreach ($roles as $rol)
                @if ($usuario->roles->contains($rol))
                    <input type="checkbox" name="rol[]" value="{{ $rol->id }}" id="rol_{{ $rol->id }}" checked><label for="rol_{{ $rol->id }}">{{ $rol->descripcion }}</label>
                @else
                    <input type="checkbox" name="rol[]" value="{{ $rol->id }}" id="rol_{{ $rol->id }}"><label for="rol_{{ $rol->id }}">{{ $rol->descripcion }}</label>
                @endif            
            
            @endforeach
            @if ($errors->has('rol'))
                <strong>{{ $errors->first('rol') }}</strong>
            @endif
        </div>
        <button name="guardar" id="guardar" value="guardar">Guardar</button>
        <button name="cancelar" id="cancelar" value="Cancelar">Cancelar</button>
    </fieldset>   
    </form>
@endsection