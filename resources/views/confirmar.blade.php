@extends('menubackend')

@section('card-header')
Eliminar pregunta
@endsection

@section('card-body')
    <form action="/preguntas/borrar/{{ $pregunta->id }}" method="POST">
    {{ csrf_field() }}
        <ul>
        <li>Pregunta: {{ $pregunta->pregunta }}</li>
        <li>Respuesta 1: {{ $respuestas[0]->respuesta }}</li>
        <li>Respuesta 2: {{ $respuestas[1]->respuesta }}</li>
        </ul>
        <p>¿Seguro desea eliminar la pregunta?</p>
        <button name="eliminar" id="eliminar" value="Eliminar">Eliminar</button>
        <button name="cancelar" id="cancelar" value="Cancelar">Cancelar</button>
    </form>
@endsection