@extends('menubackend')

@section('card-header')
Casilleros del Tablero
@endsection

@section('card-body')
    <div>
        @isset ($mensaje)
            <p>{{ $mensaje }}</p>
        @endisset
    </div>

<form action="/casilleros/guardar" method="POST">
    {{ csrf_field() }}
    <fieldset>
    <table class="tablapreg">
        <thead>
            <th class="centrado">Casillero</th>
            <th class="centrado">¿Tiene Pregunta?</th>
            <th class="centrado">Avanzar</th>
            <th class="centrado">Retroceder</th>
        </thead>
        <tbody>
            @foreach ($casilleros as $casillero)
            @if ($casillero->id <= 40)                    
            <tr>
                <td class="centrado">{{$casillero->casillero}}</td>
                
                @if ($casillero->tiene_pregunta) 
                    <td class="centrado">
                    <input type="checkbox" name="tiene_pregunta[{{$casillero->id}}]" value="{{ $casillero->tiene_pregunta }}" id="tiene_pregunta[{{$casillero->id}}]" checked onchange="cambiar({{$casillero->id}})">
                    </td>
                    <td><input type="number" name="avanza[{{$casillero->id}}]" value="{{$casillero->avanza}}" id="avanza[{{$casillero->id}}]" min="0" max="5"></td>
                    <td><input type="number" name="retrocede[{{$casillero->id}}]" value="{{$casillero->retrocede}}" id="retrocede[{{$casillero->id}}]" min="0" max="5"></td>
                @else
                    <td class="centrado">
                    <input type="checkbox" name="tiene_pregunta[{{$casillero->id}}]" value="{{ $casillero->tiene_pregunta }}" id="tiene_pregunta[{{$casillero->id}}]" onchange="cambiar({{$casillero->id}})">
                    </td>
                    <td><input class="oculta" type="number" name="avanza[{{$casillero->id}}]" value="{{$casillero->avanza}}" id="avanza[{{$casillero->id}}]" min="0" max="5"></td>
                    <td><input class="oculta" type="number" name="retrocede[{{$casillero->id}}]" value="{{$casillero->retrocede}}" id="retrocede[{{$casillero->id}}]" min="0" max="5"></td>
                @endif
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
    <br>
    <button name="guardar" id="guardar" value="guardar">Guardar</button>
    <button name="cancelar" id="cancelar" value="Cancelar">Cancelar</button>
    </fieldset>   
 </form>
 <script>
     function cambiar(id) {
        celda = document.getElementById("avanza["+id+"]");
        if (celda.getAttribute("class")=="oculta") {
            celda.setAttribute("class","");
        }
        else {
            celda.setAttribute("class","oculta");
        }
        celda2 = document.getElementById("retrocede["+id+"]");
        celda2.setAttribute("class",celda.getAttribute("class"));
     }
 </script>
@endsection
