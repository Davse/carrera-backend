@extends('menubackend')

@section('card-header')
Administración de preguntas
@endsection

@section('card-body')
    <a class="boton derecha" href= "/preguntas/cargar">Cargar nueva pregunta</a>
    <br>
    <br>
    <table class="tablapreg">
        <thead>
            <th>Pregunta</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </thead>
        <tbody>
        @foreach ($preguntas as $pregunta)
            <tr>
                @if ($pregunta->activa)
                    <td>{{ $pregunta->pregunta }}</td>
                    <td><a href= "/preguntas/editar/{{ $pregunta->id }}">Editar</a></td>
                    <td><a href= "/preguntas/desactivar/{{ $pregunta->id }}">Desactivar</a></td>
                @else  
                    <td class="inactiva">{{ $pregunta->pregunta }}</td>
                    <td class="inactiva">Editar</td>
                    <td><a href= "/preguntas/activar/{{ $pregunta->id }}">Activar</a></td>
                @endif
                @if ($pregunta->fue_respondida())
                    <td class="inactiva">Eliminar</td>
                @else
                    <td><a href= "/preguntas/confirmar/{{ $pregunta->id }}">Eliminar</a></td>
                @endif
                
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection