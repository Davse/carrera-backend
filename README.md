# Backend-carrera

Aplicación realizada en lenguaje php, utilizando el framework Laravel.

## Pasos para la instalación (local)

- Instalar Apache, PHP (versión 7.2) y Composer
- Crear una BD
- Crear directorio /var/www/backend y situarse allí
- Clonar este repositorio en el directorio creado
- Acceder al directorio que se creó durante el proceso de clonación
- Copiar archivo .env.example a .env y editarlo convenientemente (datos BD)
- Ejecutar `composer install`
- Ejecutar `php artisan key:generate`
- Ejecutar migration + seeders para cargar algunos datos necesarios y otros de prueba `php artisan migrate --seed`
- Crear usuario administrador utilizando el comando `php artisan createadmin`
- Cambiar propietario del directorio /var/www/backend y subdirectorios  `chown www-data /var/www/backend -R`
- Crear archivo `backend.conf` en /etc/apache2/sites-available
- Incluir en dicho archivo las siguientes líneas:

~~~  
<VirtualHost *:8000>
    #ServerName yourdomain.tld

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/backend/public

    <Directory /var/www/backend/public>
        AllowOverride All
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
~~~

- Agregar puerto en /etc/apache2/ports.conf ( `Listen 8000`)
- Ejecutar `a2ensite backend.conf`
- Ejecutar `a2enmod rewrite`
- Ejecutar `service apache2 restart`

Para utilizar la aplicación debe utilizar el navegador web de su preferencia y acceder a `localhost:8000`
