<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaAutor extends Model
{
    protected $table = 'pregunta-autor';
}
