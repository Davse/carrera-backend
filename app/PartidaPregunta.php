<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidaPregunta extends Model
{
    protected $table = 'partida-preguntas';
}
