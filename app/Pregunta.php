<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    public function creador()
    {
        return $this->belongsTo('App\User', 'autor');
    }

    public function modificador()
    {
        return $this->belongsTo('App\User', 'ultimo_modificador');
    }

    public function partidas()
    {
        return $this->belongsToMany('App\Partida', 'partida-preguntas','id_pregunta','id_partida');
    }

    public function respuestas()
    {
        return $this->hasMany('App\Respuesta', 'id_pregunta');
    }

    public function fue_respondida()
    {
        return !$this->partidas->isEmpty();
    }
}
