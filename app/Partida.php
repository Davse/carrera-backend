<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    public function preguntas()
    {
        return $this->belongsToMany('App\Pregunta', 'partida-preguntas','id_partida','id_pregunta');
    }
}
