<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function preguntas()
    {
        return $this->hasMany('App\Pregunta','autor');
    }
    public function modificaciones()
    {
        return $this->hasMany('App\Pregunta','ultimo_modificador');
    }

    public function roles()
    {
        return $this
            ->belongsToMany('App\Rol','usuario-rol','id_usuario','id_rol')
            ->withTimestamps();
    }

    public function tienepermiso($permiso)
    {
        foreach ($this->roles as $role) {
            if ($role->tienepermiso($permiso)){
                return true;
            }
        }
        return false;
    }

    public function validarpermiso($permiso)
    {
        if ($this->tienepermiso($permiso)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

}
