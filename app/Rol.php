<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';

    public function users()
    {
        return $this
            ->belongsToMany('App\User', 'usuario-rol', 'id_rol', 'id_usuario')
            ->withTimestamps();
    }

    public function permisos()
    {
        return $this->belongsToMany('App\Permiso', 'rol-permisos','id_rol','id_permiso');
    }

    public function tienepermiso($permiso)
    {
        if ($this->permisos()->where('nombre', $permiso)->first()) {
            return true;
        }
        return false;
    }
}
