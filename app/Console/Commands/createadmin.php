<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Rol;

class createadmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear usuario administrador';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [];
        $data['name'] = $this->ask('Ingrese nombre');
        $data['email'] = $this->ask('Ingrese e-mail');
        $data['password'] = $this->secret('Ingrese clave');
        $data['password_confirmation'] = $this->secret('Ingrese clave nuevamente');
        $validador = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        while ($validador->fails()) {
            foreach ($validador->errors()->all() as $error) {
                $this->error($error);
            } 
            // $data = [];
            $data['name'] = $this->ask('Ingrese nombre', $data['name']);
            $data['email'] = $this->ask('Ingrese e-mail', $data['email']);
            $data['password'] = $this->secret('Ingrese clave');
            $data['password_confirmation'] = $this->secret('Ingrese clave nuevamente');
            $validador = Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ]);
        }

        $rol_admin = Rol::where('nombre', 'admin')->first();

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        if ($user->save()) {
            $user->roles()->attach($rol_admin);
            $this->info('Usuario creado exitosamente');
        }
        else {
            $this->error('Acción fallida');
        }      
    }
}
