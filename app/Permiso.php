<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Rol;

class Permiso extends Model
{
    public function roles()
    {
        return $this
            ->belongsToMany('App\Rol','rol-permisos','id_permiso','id_rol')
            ->withTimestamps();
    }
}
