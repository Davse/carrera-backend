<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
use DB;
use App\Http\Requests;
use App\Partida;
use App\Http\Resources\Partida as PartidaResource;


class PartidasController extends Controller
{
    
    public function iniciar(Request $request)
    {
        try{
            //inserto partida en tabla y obtengo id
            $idPartida = DB::table('partidas')->insertGetId([
                'duracion' => 0,
                'cantidad_equipos' => $request->input('cantidad_equipos'),
                'dificultad' => $request->input('dificultad'),
                'bien_contestadas' => 0
            ]);

            //busco partida en tabla por el id devuelto
            $partida = Partida::find($idPartida);
            $casilleros = DB::table('casilleros')->get();
            foreach($casilleros as $c){
                $response_casilleros[] = [
                    'casillero' => $c->casillero,
                    'tiene_pregunta' => $c->tiene_pregunta
                ];
            }
            //return new PartidaResource($partida);
            return response()->json([
                'id_partida' => $partida->id,
                'tablero' => $response_casilleros,
                'operation' => 'success'
            ]);
        } catch(\Illuminate\Database\QueryException $ex) {
            return response()->json([
                'operation' => 'failed'
            ]);
        }
    }
    
    public function finalizar(Request $request)
    {  
        try{
            //obtengo datos del body
            $id = $request->input('id_partida');
            $duracion = $request->input('duracion');
            
            if(($id == null) || ($duracion == null)){
                return response()->json([
                    'operation' => 'failed',
                    'info' => 'missing parameters'
                ]);                
            }

            //busco la tupla
            $partida = Partida::find($id);
            if($partida == null){
                return response()->json([
                    'operation' => 'failed',
                    'info' => 'id_partida not found'
                ]);
            }
            //la modifico
            $partida->duracion = $duracion;

            //la guardo
            $partida->save();
            return response()->json(['operation' => 'success']);
        } catch(\Illuminate\Database\QueryException $ex) {
            return response()->json([
                'operation' => 'failed',
                'info' => 'missing parameters'
            ]);
        }
    }
}
