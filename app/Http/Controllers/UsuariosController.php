<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Rol;

class UsuariosController extends Controller
{
    public function usuarios()
    {
        \Auth::user()->validarpermiso('admin-usuario');
        $usuarios = User::all();
        $rol_admin = Rol::where('nombre', 'admin')->first();
        return view('usuario/usuarios', compact('usuarios','rol_admin'));
    }

    public function cargar()
    {
        \Auth::user()->validarpermiso('admin-usuario');
        $roles = Rol::all();
        return view('usuario/cargar', compact('roles'));
    }

    public function guardar(Request $request, $id=null)
    {
        \Auth::user()->validarpermiso('admin-usuario');
        if (request('cancelar')!= null) {
            return redirect ('/usuarios');
        }
        if ($id === null) {
            $usuario = new User();
            $datos_limpios = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'rol' => ['required'],
            ]);
        } else {
            $usuario = User::find($id);
            $datos_limpios = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'rol' => ['required'],
            ]);
        }
            $usuario->name = $datos_limpios['name'];
            $usuario->email = $datos_limpios['email'];
            if ($id === null) {
                $usuario->password = Hash::make($datos_limpios['password']);
            }
            $usuario->save();
            $rol_user = Rol::find(request('rol'));
            $usuario->roles()->sync($rol_user);
            return redirect ('/usuarios');
     }

     private function set_activa($id, $valor)
     {
        $usuario = User::find($id);
        $rol_admin = Rol::where('nombre', 'admin')->first();
        if ($usuario != null) {
            if (!$usuario->roles->contains($rol_admin)) {
                $usuario->activo = $valor;
                $usuario->save();         
            }
        }
        return redirect ('/usuarios');
     }
 
     public function activar($id)
     {
         \Auth::user()->validarpermiso('admin-usuario');
         return $this->set_activa($id, true);
     }
 
     public function desactivar($id)
     {
         \Auth::user()->validarpermiso('admin-usuario');
         return $this->set_activa($id, false);
     }

     public function editar($id)
     {
        \Auth::user()->validarpermiso('admin-usuario');
        $usuario = User::find($id);
        $roles = Rol::all();
        if ($usuario === null) {
            return redirect ('/usuarios');
        }
        else {
            return view('usuario/editarusu', compact('usuario','roles'));
        }
     }
}
