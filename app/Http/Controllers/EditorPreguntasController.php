<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Pregunta;
use App\Respuesta;
use App\PreguntaAutor;
use App\PreguntaPregunta;

class EditorPreguntasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function preguntas()
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        $preguntas = Pregunta::all();
        return view('preguntas', compact('preguntas'));
    }

    public function cargar()
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        return view('cargar');
    }

    public function guardar(Request $request, $id=null)
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        if (request('cancelar')!= null) {
            return redirect ('/preguntas');
        }
        if ($id === null) {
            $pregunta = new Pregunta();
            $respuesta = new Respuesta();
            $respuesta2 = new Respuesta();  
            $pregunta->autor = \Auth::user()->id;
            $pregunta->ultimo_modificador = \Auth::user()->id; 
            $pregunta->activa = true;
        } else {
            $pregunta = Pregunta::find($id);
            $respuestas = Respuesta::where('id_pregunta',$id)->get();   
            $respuesta = $respuestas[0];
            $respuesta2 = $respuestas[1];
            $pregunta->ultimo_modificador = \Auth::user()->id;
        }
        $datos_limpios = $request->validate([
            'pregunta' => 'required',
            'dificultad' => [
                'required',
                Rule::in(['baja', 'media','dificil']),
            ],
            'respuesta1' => 'required|min:5',
            'respuesta2' => 'required|min:5',
            'respuesta'=> 'required|between:1,2'
        ]);

            $pregunta->pregunta = $datos_limpios['pregunta'];
            $pregunta->dificultad = $datos_limpios['dificultad'];
            if ($id === null) {
                $pregunta->veces_bien_contestada = 0;
            }
            $pregunta->save();

            if ($id === null) {
                $id = \App\Pregunta::latest()->first()->id;
            }
            $respuesta->id_pregunta = $id;
            $respuesta->respuesta = $datos_limpios['respuesta1'];
            if ($datos_limpios['respuesta']==1) {
                $respuesta->es_correcta = true;
            } else {
                $respuesta->es_correcta = false;
            };
            $respuesta->save();

            $respuesta2->id_pregunta = $id;
            $respuesta2->respuesta = $datos_limpios['respuesta2'];
            if ($datos_limpios['respuesta']==2) {
                $respuesta2->es_correcta = true;
            } else {
                $respuesta2->es_correcta = false;
            };
            $respuesta2->save();

            return redirect ('/preguntas');
     }

   public function editar($id)
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        $pregunta = Pregunta::find($id);
        $respuestas = Respuesta::where('id_pregunta',$id)->get();         
        return view('editar',compact('pregunta','respuestas'));
    }

    public function confirmar($id)
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        $pregunta = Pregunta::find($id);
        if ($pregunta == null) {
            return redirect ('/preguntas');
        } else {
            $respuestas = $pregunta->respuestas;         
            return view('confirmar',compact('pregunta','respuestas'));
        }
    }

    public function borrar($id)
    {  
        \Auth::user()->validarpermiso('admin-pregunta');
        if (request('cancelar')!= null) {
            return redirect ('/preguntas');
        } else {
            $pregunta = Pregunta::find($id);
            if ($pregunta->partidas->isEmpty()) {
                foreach ($pregunta->respuestas as $respuesta){
                    $respuesta->delete();
                }
                $pregunta->delete();
                return view ('borrar',compact('pregunta','respuestas'));           
            }
            else {
                return redirect ('/preguntas');
            }
            
        }
 
    }

    private function set_activa($id, $valor)
    {
        $pregunta = Pregunta::find($id);
        if ($pregunta != null) {
            $pregunta->activa = $valor;
            $pregunta->save();         
        }
        return redirect ('/preguntas');
    }

    public function activar($id)
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        return $this->set_activa($id, true);
    }

    public function desactivar($id)
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        return $this->set_activa($id, false);
    }
}