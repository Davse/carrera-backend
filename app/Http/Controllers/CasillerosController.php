<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Casillero;

class CasillerosController extends Controller
{
    public function casilleros()
    {
        \Auth::user()->validarpermiso('admin-pregunta');
        $casilleros = Casillero::all();
        return view('casilleros', compact('casilleros'));
    }

    public function guardar(Request $request) {
        \Auth::user()->validarpermiso('admin-pregunta');
        if (request('cancelar')!= null) {
            return redirect ('/casilleros');
        }
        $casilleros = Casillero::all();
        foreach ($casilleros as $casillero) {
            if (array_key_exists ($casillero->id, $request['tiene_pregunta'])) {
                $casillero->tiene_pregunta = true;
                $casillero->avanza = $request['avanza'][$casillero->id];
                $casillero->retrocede = $request['retrocede'][$casillero->id];
            } 
            else {
                $casillero->tiene_pregunta = false;
            }
            $casillero->save();
        }
        $casilleros = Casillero::all();
        $mensaje = 'Los datos se almacenaron correctamente';
        return view('casilleros', compact('casilleros','mensaje'));
    }
}
