<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pregunta;
use App\Respuesta;
use App\PreguntaAutor;
use App\User;

class EstadisticasController extends Controller
{
    public function estadisticas()
    {
        \Auth::user()->validarpermiso('estadisticas');
        $preguntas = Pregunta::all();

        return view('estadisticas', compact('preguntas'));
    }

    public function ver($id)
    {
        \Auth::user()->validarpermiso('estadisticas');

        $pregunta = Pregunta::find($id);
        if ($pregunta == null) {
            return redirect ('/estadisticas');
        } else {
            $pregunta = Pregunta::find($id);
            $respuestas = Respuesta::where('id_pregunta',$id)->get();         
            return view('pregunta',compact('pregunta','respuestas'));
        }
    }

    public function csv()
    {
        \Auth::user()->validarpermiso('estadisticas');
        $preguntas = Pregunta::all();

        header('Content-Disposition: attachment; filename="export.csv"');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary");
        header("\n");

        echo "ID,Pregunta,Autor,Último Modificador,Dificultad,Apariciones,Bien Contestada" . "\r\n";
        foreach ($preguntas as $pregunta) {
            echo $pregunta->id . ',';
            echo $pregunta->pregunta . ',';
            echo $pregunta->creador->name . ',';
            echo $pregunta->modificador->name . ',';
            echo $pregunta->dificultad . ',';
            echo $pregunta->partidas->count() . ',';
            echo $pregunta->veces_bien_contestada . "\r\n";
        }
        exit;
    }
}
