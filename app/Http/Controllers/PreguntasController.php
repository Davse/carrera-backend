<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Pregunta;
use App\Partida;
use App\Respuesta;
use App\Http\Resources\Pregunta as PreguntaResource;
use App\Http\Controllers\PreguntasController as me;

class PreguntasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = Pregunta::find($id);
        
        return new PreguntaResource($pregunta);
    }
    
    public function esCorrecta($id_pregunta){
        if($id_pregunta == null){
            return response()->json([
                'operation' => 'failed',
                'info' => 'missing parameter id_pregunta'
            ]);
        }
        $pregunta = Pregunta::find($id_pregunta);
        if($pregunta == null){
            return response()->json([
                'operation' => 'failed',
                'info' => 'id not found'
            ]);
        }
        $pregunta->veces_bien_contestada += 1;
        $pregunta->save();
        return response()->json([
            'id_pregunta' => $pregunta->id,
            'operation' => 'success'
        ]);
    }
    
    public function contestar(Request $request){
        $id_respuesta = $request->input('id_respuesta');
        $respuesta = Respuesta::find($id_respuesta);
        if($respuesta == null){
            return response()->json([
                'operation' => 'failed',
                'info' => 'id_respuesta not found'
            ]);
        }
        if($respuesta->es_correcta){
            $es_correcta = "true";
            $id_pregunta = $respuesta->id_pregunta;
            me::esCorrecta($id_pregunta);
        } else {
            $es_correcta = "false";
        }
        
        return response()->json([
            'es_correcta' => $es_correcta,
            'operation' => 'success'
        ]);
    }
    
    public function tirar(Request $request){
        //agarro los datos del body
        $id_partida = $request->input('id_partida');
        $old_nro_casillero = $request->input('casillero');
        //simulo tirada de dados
        $dado = rand(1,6);
        //$response[] = ['dado' => $dado];
        //localizo el casillero
        if($old_nro_casillero + $dado <= 40){
        $new_nro_casillero = $old_nro_casillero + $dado;
        }else{
        $new_nro_casillero = 41;    
        }    
        $new_casillero = DB::table('casilleros')
                                ->where('casillero', $new_nro_casillero)
                                ->first();
        //$response[] = ['new_casillero' => ]
        //si tiene pregunta la agrego a la respuesta
        if($new_casillero->tiene_pregunta){
            $response_pregunta = me::getPregunta($id_partida);
        } else {
            $response_pregunta = 'null';
        }
        
        return response()->json([
            'dado' => $dado,
            'new_casillero' => [
                'numero' => $new_casillero->casillero,
                'tiene_pregunta' => $new_casillero->tiene_pregunta,
                'avanza' => $new_casillero->avanza,
                'retrocede' => $new_casillero->retrocede
            ],
            'pregunta' => $response_pregunta
        ]);
        
    }
    
    public function getPregunta($id_partida)
    {   
        $partida = Partida::findOrFail($id_partida);
        $dificultad = $partida->dificultad;
        
        
        $preguntasHechasDificultad = DB::table('partida-preguntas')
                                        ->where('id_partida', $partida->id)
                                        ->get();
        
        $idsPreguntasHechasDificultad = [];
        foreach($preguntasHechasDificultad as $p){
            $idsPreguntasHechasDificultad[] = $p->id_pregunta;
        }
        
        $preguntasDificultadDisponibles = Pregunta::whereNotIn('id', $idsPreguntasHechasDificultad)
                                                    ->where('dificultad', $dificultad)
                                                    ->where('activa', true)
                                                    ->get(['id', 'pregunta', 'dificultad']);
        if(($preguntasDificultadDisponibles)->isEmpty()){
            $selected = Pregunta::where('activa', true)->inRandomOrder()->first();
        } else {
            $index = random_int(0, sizeof($preguntasDificultadDisponibles)-1); 
            $selected = $preguntasDificultadDisponibles[$index];
        }
        
        //agrego en la tabla intermedia para que registre que ya se envio esa pregunta
        DB::table('partida-preguntas')->insert([
                                        'id_partida' => $id_partida, 
                                        'id_pregunta' => $selected->id
                                        ]);
        
        //recupero respuestas
        $respuestas = DB::table('respuestas')
                                ->where('id_pregunta', $selected->id)
                                ->get(['id', 'respuesta']);
        
        //armo json con pregunta + respuestas
        $selected = $selected->toArray();
        
        foreach($respuestas as $r){
            $selected['respuestas'][] = $r;
        }
        return $selected;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
