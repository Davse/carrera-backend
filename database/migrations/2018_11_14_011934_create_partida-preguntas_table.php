<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidaPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partida-preguntas', function (Blueprint $table) {
            
            $table->increments('id');
            $table->unsignedInteger('id_partida');
            $table->unsignedInteger('id_pregunta');
            $table->timestamps();

            $table->foreign('id_partida')->references('id')->on('partidas');
            $table->foreign('id_pregunta')->references('id')->on('preguntas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partida-preguntas');
    }
}
