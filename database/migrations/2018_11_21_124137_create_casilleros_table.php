<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasillerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casilleros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('casillero');
            $table->boolean('tiene_pregunta');
            $table->integer('avanza');
            $table->integer('retrocede');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casilleros');
    }
}
