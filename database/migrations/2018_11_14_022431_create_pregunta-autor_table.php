<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntaAutorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta-autor', function (Blueprint $table) {
            
            $table->increments('id');
            $table->unsignedInteger('id_pregunta');
            $table->unsignedInteger('autor');
            $table->unsignedInteger('ultimo_modificador');
            $table->timestamps();

            $table->foreign('id_pregunta')->references('id')->on('preguntas');
            $table->foreign('autor')->references('id')->on('users');
            $table->foreign('ultimo_modificador')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta-autor');
    }
}
