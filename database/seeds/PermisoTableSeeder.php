<?php

use Illuminate\Database\Seeder;
use App\Rol;
use App\Permiso;

class PermisoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol_user = Rol::where('nombre', 'usuario')->first();
        $rol_admin = Rol::where('nombre', 'admin')->first();

        $permiso = new Permiso();
        $permiso->nombre = 'admin-usuario';
        $permiso->descripcion = 'Permitir ABM usuarios';
        $permiso->save();
        $permiso->roles()->attach($rol_admin);

        $permiso = new Permiso();
        $permiso->nombre = 'admin-pregunta';
        $permiso->descripcion = 'Permitir ABM preguntas';
        $permiso->save();
        $permiso->roles()->attach($rol_admin);
        $permiso->roles()->attach($rol_user);

        $permiso = new Permiso();
        $permiso->nombre = 'estadisticas';
        $permiso->descripcion = 'Permitir ver estadisticas';
        $permiso->save();
        $permiso->roles()->attach($rol_admin);
        $permiso->roles()->attach($rol_user);
    }
}
