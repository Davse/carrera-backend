<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Rol;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol_user = Rol::where('nombre', 'usuario')->first();
        $rol_admin = Rol::where('nombre', 'admin')->first();

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('secreto');
        $user->save();
        $user->roles()->attach($rol_admin);

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@example.com';
        $user->password = bcrypt('secreto');
        $user->save();
        $user->roles()->attach($rol_user);
    }
}
