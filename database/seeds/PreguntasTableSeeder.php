<?php

use Illuminate\Database\Seeder;
use App\Pregunta;
use App\Respuesta;

class PreguntasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json = File::get("database/data/preguntas.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          Pregunta::create(array(
            'id' => $obj->id,
            'pregunta' => $obj->pregunta,
            'dificultad'=> $obj->dificultad,
            'veces_bien_contestada' => $obj->veces_bien_contestada,
            'autor' => $obj->autor,
            'ultimo_modificador' => $obj->ultimo_modificador,
            'activa'=> $obj->activa
          ));
        }

        $json2 = File::get("database/data/respuestas.json");
        $data2 = json_decode($json2);
        foreach ($data2 as $obj) {
          Respuesta::create(array(
            'id' => $obj->id,
            'id_pregunta' => $obj->id_pregunta,
            'respuesta'=> $obj->respuesta,
            'es_correcta' => $obj->es_correcta
          ));
        }
/*
        for ($i=1; $i<=50; $i++) {
            $pregunta = new Pregunta();
            $pregunta->pregunta = ('Pregunta '.$i);
            if (rand(1,3)==1) {
                $dificultad = 'baja';
            } else {
                if (rand(1,3)==2) {
                    $dificultad = 'media';
                } else {
                    $dificultad = 'dificil';
                }
            }
            $pregunta->dificultad = $dificultad;
            $pregunta->veces_bien_contestada = 0;
            $pregunta->autor = 1;
            $pregunta->ultimo_modificador = 1;
            $pregunta->activa = true;
            $pregunta->save();

            $respuesta1 = new Respuesta();
            $respuesta2 = new Respuesta();

            $respuesta1->id_pregunta = $i;
            $respuesta1->respuesta = 'Respuesta A';
            if ($i % 2 == 0) {
                $respuesta1->es_correcta = true;
            } else {
                $respuesta1->es_correcta = false;
            }
            $respuesta1->save();

            $respuesta2->id_pregunta = $i;
            $respuesta2->respuesta = 'Respuesta B';
            if ($i % 2 == 0) {
                $respuesta2->es_correcta = false;
            } else {
                $respuesta2->es_correcta = true;
            }
            $respuesta2->save();
        }
    */
    }   
}
