<?php

use Illuminate\Database\Seeder;
use App\Casillero;

class CasillerosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=41; $i++) {
            $casillero = new Casillero();
            $casillero->casillero = $i;
            $casillero->tiene_pregunta = (($i % 4) == 0);
            $casillero->avanza = 1;
            $casillero->retrocede = 1;
            $casillero->save();
        }
    }
}
