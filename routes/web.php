<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/preguntas', 'EditorPreguntasController@preguntas');

Route::get('/preguntas/cargar', 'EditorPreguntasController@cargar');

Route::post('/preguntas/guardar', 'EditorPreguntasController@guardar');

Route::post('/preguntas/guardar/{id}', 'EditorPreguntasController@guardar');

Route::get('/preguntas/editar/{id}', 'EditorPreguntasController@editar');

Route::get('/preguntas/confirmar/{id}', 'EditorPreguntasController@confirmar');

Route::post('/preguntas/borrar/{id}', 'EditorPreguntasController@borrar');

Route::get('/estadisticas', 'EstadisticasController@estadisticas');

Route::get('/usuarios', 'UsuariosController@usuarios');

Route::get('/usuarios/cargar', 'UsuariosController@cargar');

Route::post('/usuarios/guardar', 'UsuariosController@guardar');

Route::get('/preguntas/activar/{id}', 'EditorPreguntasController@activar');

Route::get('/preguntas/desactivar/{id}', 'EditorPreguntasController@desactivar');

Route::get('/estadisticas/pregunta/{id}', 'EstadisticasController@ver');

Route::get('/estadisticas/csv', 'EstadisticasController@csv');

Route::get('/usuarios/editar/{id}', 'UsuariosController@editar');

Route::get('/usuarios/activar/{id}', 'UsuariosController@activar');

Route::get('/usuarios/desactivar/{id}', 'UsuariosController@desactivar');

Route::post('/usuarios/guardar/{id}', 'UsuariosController@guardar');

Route::get('/casilleros', 'CasillerosController@casilleros');

Route::post('/casilleros/guardar', 'CasillerosController@guardar');