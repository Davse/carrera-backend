<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//estas dos quedaron fuera de la aplicacion
//Route::get('/pregunta/{id}', 'PreguntasController@show');
//Route::post('esCorrecta', 'PreguntasController@esCorrecta');
//Route::get('pregunta/{id_partida}', 'PreguntasController@getPregunta');

Route::any('tirar', 'PreguntasController@tirar');
Route::post('contestar', 'PreguntasController@contestar');

Route::post('/iniciar', 'PartidasController@iniciar');
Route::post('/finalizar', 'PartidasController@finalizar');
